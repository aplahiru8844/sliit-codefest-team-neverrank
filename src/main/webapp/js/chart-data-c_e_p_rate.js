window.onload = function() {
    // line chart-01 data for Current Error Processing Rate
    new Chart(document.getElementById("line-chart-01"), {
        type: 'line',
        data: {
          labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050], //x Values
          datasets: [{ 
              data: [86,114,106,106,107,111,133,221,783,2478], //y Values
              label: "Rate",
              borderColor: "#3e95cd",
              fill: false
          }
          ]
        },
        options: {
          title: {
            display: true,
            text: 'STEP 01'
          }
        }
      });
     
      // line chart-02 data for Current Error Processing Rate
      new Chart(document.getElementById("line-chart-02"), {
        type: 'line',
        data: {
          labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050], //x Values
          datasets: [
            { 
              data: [282,350,411,502,635,809,947,1402,3700,5267], //y Values
              label: "Rate",
              borderColor: "#8e5ea2",
              fill: false
            }
          ]
        },
        options: {
          title: {
            display: true,
            text: 'STEP 02'
          }
        }
      });
    
    // line chart-03 data for Current Error Processing Rate
    new Chart(document.getElementById("line-chart-03"), {
      type: 'line',
      data: {
        labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050], //x Values
        datasets: [
          { 
            data: [282,350,411,502,635,809,947,1402,3700,5267], //y Values
            label: "Rate",
            borderColor: "#3cba9f",
            fill: false
          }
        ]
      },
      options: {
        title: {
          display: true,
          text: 'STEP 03'
        }
      }
    });
    
    // line chart-04 data for Current Error Processing Rate
    new Chart(document.getElementById("line-chart-04"), {
      type: 'line',
      data: {
        labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050], //x Values
        datasets: [
          { 
            data: [282,350,411,502,635,809,947,1402,3700,5267], //y Values
            label: "Rate",
            borderColor: "#e8c3b9",
            fill: false
          }
        ]
      },
      options: {
        title: {
          display: true,
          text: 'STEP 04'
        }
      }
    });
    
    // line chart-05 data for Current Error Processing Rate
    new Chart(document.getElementById("line-chart-05"), {
      type: 'line',
      data: {
        labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050], //x Values
        datasets: [
          { 
            data: [282,350,411,502,635,809,947,1402,3700,5267], //y Values
            label: "Rate",
            borderColor: "#c45850",
            fill: false
          }
        ]
      },
      options: {
        title: {
          display: true,
          text: 'STEP 05'
        }
      }
    });
    
    
    
    
    
    
    
    
    
    }