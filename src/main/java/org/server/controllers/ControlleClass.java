package org.server.controllers;
import org.server.models.Product;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("data")
public class ControlleClass {
    private long millis;
    private Insert insertOBJ=new Insert();
    @RequestMapping(value = "{count}/{isdefective}",method = RequestMethod.GET )
    public String home(@PathVariable int count,@PathVariable String  isdefective){

        millis=System.currentTimeMillis();
        Product product=new Product();
        product.setSensorId(1);
        product.setProductCount(count);
        product.setStatus(isdefective);
        product.setMillies(millis);

        insertOBJ.insertProductDetails(product);

        return "index";
    }
}
