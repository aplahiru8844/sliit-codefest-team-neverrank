package org.server.models;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "productdetails")
public class Product {
    @Id
    @Column(name = "id")
    private int id;
    @Column(name ="sensorId")
    private int sensorId;
    @Column(name = "productCount")
    private long productCount;
    @Column(name = "status")
    private  String status;
    @Column(name = "date")
    private  String date;
    @Column(name = "time")
    private String time;
    @Column(name = "millies")
    private long millies;


    public int getSensorId() {
        return sensorId;
    }

    public void setSensorId(int senserId) {
        this.sensorId = senserId;
    }

    public long getProductCount() {
        return productCount;
    }

    public void setProductCount(long productCount) {
        this.productCount = productCount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public long  getMillies() {
        return millies;
    }

    public void setMillies(long millies) {
        this.millies = millies;
    }


}
